define([], function(){
//////////////////////////////////////////////////////////////////////////////

// initial PGP message delayed sending

var openedTabs = {};

chrome.tabs.onUpdated.addListener(function(tabID, changeInfo, tab){
    if(!openedTabs[tabID]) return;
    openedTabs[tabID]._onUpdated(changeInfo);
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if(undefined == request.c2b) return;
    var tabID = request.c2b;
    if(!openedTabs[tabID]) return;
    openedTabs[tabID]._onMessage(request.payload); 
});

chrome.windows.onRemoved.addListener(function(windowID){
    var found = false;
    for(var i in openedTabs){
        if(windowID == openedTabs[i].windowID){
            openedTabs[i]._onRemove();
            found = true;
            break;
        }
    }
    if(found) delete openedTabs[i];
});


function PopupManager(w, options, eventListeners){
    var self = this;
    options = options || {};
    eventListeners = eventListeners || {};

    this.tabID = w.tabs[0].id;
    this.windowID = w.id;

    var tabLoaded = false;
    this._onUpdated = function(changeInfo){
        if('complete' == changeInfo.status){
            tabLoaded = true;
            self.send();
        }
    };

    var sendingQueue = [];
    
    this.send = function(message){
        if(message) sendingQueue.push(message);
        if(!tabLoaded) return;
        while(sendingQueue.length > 0){
            chrome.tabs.sendMessage(self.tabID, {
                b2c: self.tabID,
                pgpMessage: sendingQueue.shift()
            });
        }
    }

    this._onRemove = function(e){
        if(eventListeners.onRemove) eventListeners.onRemove();
    }
    
    this._onMessage = function(e){
        var event = e.event, data = e.data;
        if('result' == event){
            if(eventListeners.onResult) eventListeners.onResult(data);
            if(options.autoClose){
                chrome.windows.remove(self.windowID);
                self._onRemove();
            }
        }
    }

    return this;
}

// proceed with BrGP

function isPGPProduct(s){
    if(
        (
            s.indexOf('-----BEGIN PGP MESSAGE-----') >= 0 &&
            s.indexOf('-----END PGP MESSAGE-----') >= 0
        ) || (
            s.indexOf('-----BEGIN PGP SIGNED MESSAGE-----') >= 0 &&
            s.indexOf('-----BEGIN PGP SIGNATURE-----') >= 0 &&
            s.indexOf('-----END PGP SIGNATURE-----') >= 0
        )
    )
        return true;
    return false;
}

function proceedWithBrGP(text, options){
    var options = options || {};
    
    if(
        'read' == options.mode || 
        (undefined == options.mode && isPGPProduct(text))
    ){
        url = 'main.html#popup-read';
    } else {
        url = 'main.html#popup-compose';
    }

    var promise = new Promise(function(fulfill, reject){
        if(Object.keys(openedTabs).length >= 5){
            return reject('too-many-sessions');
        }

        // calculate popup position

        var screenWidth = 1024, screenHeight = 768,
            windowWidth = 700, windowHeight = 500;
        var pLeft, pTop;

        if(window.screen.width){
            screenWidth = window.screen.width;
            screenHeight = window.screen.height;
        }

        pLeft = (screenWidth - windowWidth) / 2;
        pTop = (screenHeight - windowHeight) / 2;
        if(pLeft < 0) pLeft = 0;
        if(pTop < 0) pTop = 0;

        // create popup

        chrome.windows.create({
            url: url,
            type: 'popup',
            width: windowWidth,
            height: windowHeight,
            left: pLeft,
            top: pTop,
        }, function(w){
            var callbacks = {};
            callbacks.onRemove = function(){
                reject('closed');
            }
            callbacks.onResult = function(result){
                fulfill(result);
            }

            var popup = new PopupManager(w, {
                autoClose: options.autoClose || false,
            }, callbacks);
            openedTabs[popup.tabID] = popup;
            popup.send({
                data: text,
                id: options.id || '',
            });
        });
    });
    return promise;
}

return proceedWithBrGP;

//////////////////////////////////////////////////////////////////////////////
});

/*
 * Program API for initiating BrGP
 * ===============================
 *
 * Allowed callers are currently:
 *   * Websites: neoatlantis.org, neoatlantis.info, chaobai.li
 *   * And all extensions
 *
 * Send a message to this extension using `chrome.runtime.sendMessage`, with
 * a input like:
 *
 *   { api: "<API NAME>", data: <DATA> }
 *
 * <API NAME> can be either `compose-with-plaintext` or `read-ciphertext`. For
 * both cases, <DATA> must be a string giving the suggested input.
 */

require([
    'proceedWithBrGP',
], function(
    proceedWithBrGP
){
//////////////////////////////////////////////////////////////////////////////

function composeWithPlaintext(data, response){
    if(typeof data != 'string'){
        return response({ error: { invalidInput: true }});
    }
    var promise = proceedWithBrGP(data, { mode: 'compose', autoClose: true });
    promise.then(function(result){
        response({ result: result.data });
    }, function(error){
        response({ error: error });
    });
}

function readCiphertext(data, response){
    if(typeof data != 'string'){
        return response({ error: { invalidInput: true }});
    }
    proceedWithBrGP(data, { mode: 'read', autoClose: false });
    return response({ accepted: true });
}



chrome.runtime.onMessageExternal.addListener(function(req, sender, response){
    var apiName = req.api || null, data = req.data || null;
    var apis = {
        'compose-with-plaintext': composeWithPlaintext,
        'read-ciphertext': readCiphertext,
    };

    if((typeof apiName != 'string') || apis[apiName] == undefined){
        return response({ error: { invalidAPIName: true }});
    }

    apis[apiName](data, response);
    return true; // tell chrome to wait async response
});

//////////////////////////////////////////////////////////////////////////////
});

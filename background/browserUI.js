require([
    'proceedWithBrGP',
], function(
    proceedWithBrGP
){
//////////////////////////////////////////////////////////////////////////////

// Context Menu "Proceed with BrGP"

chrome.contextMenus.create({
    "title": "Proceed with BrGP",
    "contexts": ["selection"],
    "onclick": function(e, tab){
        var text0 = e.selectionText.trim();
        if(!text0) return;
        chrome.tabs.executeScript(tab.id, {
            code: 'window.getSelection().toString();',
        }, function(text){
            var url;
            if(!text) 
                text = text0;
            else
                text = text[0].trim();
            if(!text) text = text0;
            //console.log(arguments);
            //console.log('selection', e);
            proceedWithBrGP(text, { autoClose: false });
        });
    }, 
});

// Icon button for main console

chrome.browserAction.onClicked.addListener(function(tab){
    window.open('main.html', '_blank');
});

//////////////////////////////////////////////////////////////////////////////
});

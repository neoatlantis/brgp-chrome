BrGP for Chrome
===============

Browser based PGP, specifically for chrome browsers.

This extension will add a console to your browser, in which PGP key management
as well as composing and reading PGP messages can be accomplished without
installing any program to your local computer.

Furthermore, upon selecting any text while browsering the Internet and right
click, a "Proceed with BrGP" menu item will be added. Plaintext and ciphertext
can be redirected to get PGP'ed thus easily.

This extension wraps the generic BrGP compiled code, whose source can be found
at <https://bitbucket.org/neoatlantis/brgp>.


API
---

(Added since 0.0.6) Websites(currently including only `neoatlantis.org` and
`neoatlantis.info`, all my sites :P) and all extensions can now initiate a
popup asking user to compose or read a piece of message, by using code:

```
chrome.runtime.sendMessage("<MY EXTENSION ID>", {
    api: "<API NAME>",
    data: "<YOUR TEXT HERE>",
}, function(response){
    // YOUR CODE HERE
});
```

`<API NAME>` must be either "compose-with-plaintext" or "read-ciphertext".

### API:compose-with-plaintext

When a piece of message is presented to user using this API to ask for
encryption/signature, `response` will carry the result to caller script.
`response` can be either an object of `{ result: "..." }` or `{ error: "reason"
}`.

User's popup window closes automatically once a result is generated.

### API:read-ciphertext

When calling this API, a PGP-generated input is assumed. The user will be
instructed to decrypt(if necessary) this input, and validation of any signature
will be tried.

User's popup window will not close automatically in such case. `response` will
also not transmit any decrypted message to outer script.

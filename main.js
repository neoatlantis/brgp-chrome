/*
 * A Wrapper Page for Communicating with BrGP page
 */

var pgpmessage = null, tabID = null;

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if(undefined == request.b2c) return;
    if(!tabID) tabID = request.b2c;
    if(request.pgpMessage) pgpmessage = request.pgpMessage;
});

document.addEventListener('DOMContentLoaded', function() {
    var prefix = 'openpgp-';
    var storage = chrome.storage.local;

    window.addEventListener('message', function(e){
        console.log("Background message", e);
        e = e.data;
        if(!e) return;
        
        if('dump' == e.event){
            var dump = e.data;
            var set = {};
            for(var name in dump){
                set = {};
                set[name] = dump[name];
                try{
                    storage.set(set);
                } catch(e){
                    console.error(e, set);
                }
            }
            (function(dumped){
                storage.get(null, function(items){
                    var removes = [];
                    for(var name in items){
                        if(!dumped[name]) removes.push(name);
                    }
                    storage.remove(removes);
                });
            })(dump);
            return;
        }
        
        if('sync' == e.event){
            // extract storage and init
            function onAllKeysExtracted(items){
                var data = {};
                for(var id in items){
                    if(id.slice(0, prefix.length) !== prefix) continue;
                    data[id] = items[id];
                }
                console.log("Reload with items", items);
                document.getElementById('iframe').contentWindow.postMessage({
                    event: 'init',
                    data: data,
                }, '*');
            }
            storage.get(null, onAllKeysExtracted);
            if(pgpmessage){
                console.log("Init with selection message");
                document.getElementById('iframe').contentWindow.postMessage({
                    event: 'pgpmessage',
                    data: pgpmessage,
                }, '*');
            }
            return;
        }

        // otherwise, transmit the event back to background script, which will
        // be received by PopupManager defined in `background/proceedWithBrGP`.
        chrome.runtime.sendMessage({
            c2b: tabID,
            payload: e,
        });
    });
    
    document.getElementById('iframe').src = 
        "/brgp/index.html" + window.location.hash;
});


zip: brgp/* brgp/docs/* background/*.js *.html *.json *.js resource/*
	rm -f package.zip
	zip -r package.zip \
	    brgp \
	    background \
	    resource \
	    *.js \
	    *.html \
	    *.json
